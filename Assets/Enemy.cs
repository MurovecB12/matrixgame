﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public int health;
	public int score;
	public Napad[] napadi;
	public GameObject prefab; 
	int atk = 10;
	int def = 5;
	public int stNapadov;
	
	
	void Awake () {
		health = 100;
		stNapadov = 5;
		napadi = new Napad[stNapadov];
		GameObject temp;
		
		for (int i = 0; i < stNapadov; i++) {
			temp = Instantiate (prefab, this.transform.position, Quaternion.identity) as GameObject;
			temp.GetComponent<Napad> ().setAtk (Random.Range (0, atk));
			temp.GetComponent<Napad> ().setDef (Random.Range (-Mathf.RoundToInt (def / 4f), def));
			temp.GetComponent<Napad> ().setRandTip ();
			temp.transform.SetParent (this.transform);
			temp.name = "B" + i.ToString();
		}
	}
}
