﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Napad : MonoBehaviour {


	public enum AttackType{Defensive,Melee,Ranged,Magic,Length};

	public int tip;
	public int atk;
	public int def;


	public Napad( int atk=10, int def=5, int tip = 0){
		atk = Random.Range (0, atk);
		def = Random.Range (-Mathf.RoundToInt(def/4f), def);
		setRandTip ();
	}

	public void setAtk(int newAtk){
		atk = newAtk; 
	}
	public void setDef(int newDef){
		def = newDef;
	}
	public void setRandTip(){
		tip =Random.Range(0,(int)AttackType.Length);
	}

	public void onClick(){
		int bestPlayerDmg = 0;
		int bestEnemyDmg = 0;
		GameObject enemy = GameObject.Find ("enemy");
		GameObject player = GameObject.Find ("player");

		for (int i=0; i < enemy.GetComponent<Enemy>().stNapadov; i++) {
			int pDmg = izracunajDMG(enemy.transform.GetChild(i).GetComponent<Napad>());
			int eDmg = enemy.transform.GetChild(i).GetComponent<Napad>().izracunajDMG(this);
			if(eDmg > bestEnemyDmg){
				bestEnemyDmg = eDmg;
				bestPlayerDmg = pDmg;
			}
			if(eDmg == bestEnemyDmg){
				if(pDmg < bestPlayerDmg){
					bestEnemyDmg = eDmg;
					bestPlayerDmg = pDmg;
				}
			}
		}

		player.GetComponent<Player> ().health -= bestEnemyDmg;
		enemy.GetComponent<Enemy> ().health -= bestPlayerDmg;
	}

	public int izracunajDMG(Napad enNapad){
		if (this.tip == enNapad.tip + 1 || (this.tip == 0 && enNapad.tip==3 )) {
			return 2*this.atk -enNapad.def;	
		}
		else{
			return this.atk -enNapad.def;
		}
	}


}
