﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	GameObject scr;
	GameObject player;
	GameObject enemy;
	public GameObject gumbPrefab; 
	public GameObject matrikaPrefab; 

	public int[,] izracunanaMatrika;


	// Use this for initialization
	void Start () {
		scr = GameObject.Find("screen");
		player= GameObject.Find("player");
		enemy = GameObject.Find("enemy");	
		ustvariMatriko ();
		posodobiScorePlatno ();
	}


	public Color setColor(int i){
		if (i == 0) {
			return new Color(0.5f,0.5f,0.5f,1);
		}
		if (i == 1) {
			return new Color(0.5f,0.5f,0,1);
		}
		if (i == 2) {
			return new Color(0,0.5f,0.5f,1);
		}
		if (i == 3) {
			return new Color(1,0,1,1);
		}
		else {
			return new Color(0,0,0,1);
		}
	}


	void ustvariMatriko(){
		int stNapadovPlayer = player.GetComponent<Player> ().stNapadov;
		int stNapadovEnemy = enemy.GetComponent<Enemy> ().stNapadov;
		GameObject temp;
		Transform child;

		// gremo samo cez tri ta glavne dele matrike izpisa 
		for (int i=0; i < scr.transform.childCount; i++) {
			child = scr.transform.GetChild(i);

			// vrednosti v matriki "1/4"
			if (child.name == "matrikaNapadov"){
				for(int n=0; n < stNapadovPlayer; n++){
					for( int m=0; m< stNapadovEnemy; m++){
						temp = Instantiate (matrikaPrefab, child.transform.position, Quaternion.identity) as GameObject;
						temp.transform.SetParent (child.transform);
						Napad plNapad = player.transform.GetChild(n).GetComponent<Napad>();
						Napad enNapad = enemy.transform.GetChild(m).GetComponent<Napad>();
						temp.GetComponentInChildren<Text>().text = (plNapad.izracunajDMG(enNapad)).ToString() + "/" + (enNapad.izracunajDMG(plNapad)).ToString() ;
					}
				}
			}

			// gumbi za enemy
			if (child.name == "EnemyNapadi"){
				for (int j=0; j<stNapadovEnemy; j++) {
					temp = Instantiate (gumbPrefab, child.transform.position, Quaternion.identity) as GameObject;
					temp.transform.SetParent (child.transform);

					Napad enNapad = enemy.transform.GetChild(j).GetComponent<Napad>();
					temp.GetComponent<Image>().color = setColor (enNapad.tip);
					temp.name = "B" + j.ToString();
					temp.GetComponentInChildren<Text>().text = "B" + j.ToString() + "("+enNapad.tip+")"; // imamo objekt "Text" na prefabu
				}
			}

			// gumbi za player
			if (child.name == "PlayerNapadi"){
				for (int j=0; j<stNapadovPlayer; j++) {
					temp = Instantiate (gumbPrefab, child.transform.position, Quaternion.identity) as GameObject;
					temp.transform.SetParent (child.transform);

					Napad plNapad = player.transform.GetChild(j).GetComponent<Napad>();
					temp.GetComponent<Image>().color = setColor (plNapad.tip);
					temp.name = "A" + j.ToString();
					temp.GetComponentInChildren<Text>().text = "A" + j.ToString() + "("+plNapad.tip+")"; // imamo objekt "Text" na prefabu

					Button btn = temp.GetComponent<Button>();
					btn.onClick.AddListener(player.transform.GetChild(j).GetComponent<Napad>().onClick);
					btn.onClick.AddListener(posodobiScorePlatno);
				}
			}
		}
	}

	public void posodobiScorePlatno(){
		GameObject platnoIzpis = GameObject.Find("gameInfo");
		foreach (Text score in platnoIzpis.GetComponentsInChildren<Text>()) {
			if(score.name == "playerScore"){
				score.text = player.GetComponent<Player> ().health.ToString();
			}
			if(score.name == "enemyScore"){
				score.text = enemy.GetComponent<Enemy> ().health.ToString();
			}	
		}
	}



}
