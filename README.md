# README #

# Sodelujoči #
* Benjamin Medvešček Murovec

# Povzetek #
* Igra na podlagi matričnih iger iz teorije iger.

# Opis #
* Igralec izbere enega izmed napadov (A0-A7) na kar nasprotnik odgovori s svojim najboljšim(najvišjo številko) napadom (B0-B4).
* Vsak napad ima "tip napada", ki je trenutno število med 0 in 3(številka v oklepajih). Napad ima dvakratno vrednost proti nasprotniku, ki je izbrau napada, ki je točno za 1 manjši(pri tem da ima napad 0 bonus proti napadu 3).

# TO DO #
* Scaling ne deluje pravilno.
* Ko igralec pride na 0 (player/enemy score) se more igra ustaviti.